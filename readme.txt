The SETOLS pachage currently can be used to calculate the electric
conductivity of doped crystalline semiconductors in hopping regime 
as function of tempearture and applied electric field.

Examples of usage are given in the "main.cpp". Uncomment appropriate
examples and compile the program.


Current version makes calculations using mean-field model. A detailed
description of the algorithm and the model are published in
Physica B: Condensed Matter 608 (2021) 412871
https://doi.org/10.1016/j.physb.2021.412871
See also comments in the code.

Package also contains a tool (er-extract) for extraction of the the
effective hopping energy and distance  from a temperature dependence
of resistivity in hopping regime (see dedicated readme file).


More detailed information can be provided if there will be any demand.
avdonin@ifpan.edu.pl
a.avdonin.76@gmail.com
