/*
    SETOLS stands for Simulation of Electron Transport Over Localized States. Mean field approach.
    Copyright (C) 2020 Andrei Avdonin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#include <iostream> // cout
#include <string>
#include <vector>
#include <cstdlib> // rand, srand, RAND_MAX
#include <fstream> // ofstream, ifstream
#include <cmath> // exp()
#include <iomanip> // output formatting
#include <algorithm>    // std::sort
#include <string> 

using namespace std;

#include "meanfield.cpp"

/**********************************************************/
void output_rho(ofstream& f, double rho){
   f << setw(15) << setprecision(7) << 1/beta;
   f << setw(15) << setprecision(7) << alpha;
   f << setw(10) << iter_n;
   f << setw(6) << active_n;
   f << setw(15) << setprecision(7) << rho << "\n";

   cout << setw(15) << setprecision(7) << 1/beta;
   cout << setw(15) << setprecision(7) << alpha;
   cout << setw(10) << iter_n;
   cout << setw(6) << active_n;
   cout << setw(15) << setprecision(7) << rho << "\n";
}

/**********************************************************/
void calc_rho_vs_T(double Alpha, 
                   double kTmax, 
                   double kTmin, 
                   double coef,
                   string fname){
 ofstream f;
 f.open(fname.c_str());
 double kT = kTmax;
 alpha = Alpha; 
 beta = 1/kT;
 //beta = 1/kT/1.001; // test for derivative
 while (beta < 1/kTmin){
   output_rho(f, calc_rho());
   kT *= coef;
   beta = 1/kT;
   //beta = 1/kT/1.001; // test for derivative
 }
 f.close();
}

/**********************************************************/
// test of simple averaging
/**********************************************************/
void calc_typical_vs_T(double Alpha, 
                   double kTmax, 
                   double kTmin, 
                   double coef,
                   string fname){
 double e = 0;
 double r = 0;
 ofstream f;
 f.open(fname.c_str());
 double kT = kTmax;
 alpha = Alpha; 
 beta = 1/kT;
 while (beta < 1/kTmin){
   thermalize();
   calc_bij();
   calc_n_iter();   
   calc_typical(r,e);
   f << setw(15) << setprecision(7) << 1/beta;
   f << setw(15) << setprecision(7) << r;
   f << setw(15) << setprecision(7) << e << endl;
   cout << setw(15) << setprecision(7) << 1/beta;
   cout << setw(15) << setprecision(7) << r;
   cout << setw(15) << setprecision(7) << e << endl;   
   kT *= coef;
   beta = 1/kT;
 }
 f.close();
}

/**********************************************************/
void calc_rho_vs_field(double kT, 
                       double min, 
                       double max, 
                       double step,
                       string fname){
 ofstream f;
 f.open(fname.c_str());
 alpha = min;
 beta = 1/kT;
 while (alpha <= max){
   // at high field limiting the number of active nodes results in 
   // saturation of resistivity (set occ.threshold to zero)
   if (alpha>0.8) octh = 0.0;
   if (abs(alpha)<1e-6) {alpha += step; continue;} // omit 0
   output_rho(f, calc_rho());
   alpha += step;
 }
 f.close();
}

/**********************************************************/
// adjusted for 1/rho vs E^1/2 scale 
// min, max and step are for E^1/2
/**********************************************************/
void calc_rho_vs_field2(double kT, 
                       double min, 
                       double max, 
                       double step,
                       string fname){
 ofstream f;
 f.open(fname.c_str());
 double sra = min; // square root of alpha
 alpha = sra * sra;
 beta = 1/kT;
 while (sra <= max){
   // at high field limiting the number of active nodes results in 
   // saturation of resistivity (set occ.threshold to zero)
   if (alpha>0.16) octh = 0.0;
   if (abs(alpha)<1e-6) {sra += step; alpha = sra * sra; continue;} // omit alpha=0
   output_rho(f, calc_rho());
   sra += step;
   alpha = sra * sra;
 }
 f.close();
}

/**********************************************************/
/*                                                        */
/**********************************************************/
void thermalize_gradually(double kTmax, double kTmin, double coef){
 double kT = kTmax;
 while (beta < 1/kTmin){
   beta = 1/kT;
   cout << "thermalizing to kT="<< setprecision(3) << kT << ";" << endl;
   thermalize();
   if (kT*coef < kTmin) break;
   kT *= coef;
 }
}


/**********************************************************/
/*                        MAIN                            */
/**********************************************************/
int main( int argc, char** argv)
{

 aB = 1.0/3.0; // i.e aB=d0/3
 octh = 1e-10;
 ocerr = 1e-10; 
 MAX_ITER_E = 2000;
 MAX_ITER_N = 2000000;
 
 //============ export averaged DOS ========
 /*
 double kT = 0.1;
 export_dos("dos.txt", 1/kT, 500, 0.5, 100, -5.0, 5.0, 100);
 return 0;
 */

 // init random seed to have repeatable or non-repeatable results
 //srand (time(NULL));
 //srand (68365424); //use this to have repeatable cell
 create_cell(500, 0.5);



 //============ calc. temperature dependence ===========
 //calc_rho_vs_T(0.0001, 2.5, 0.02, 0.95, "rho_vs_T.txt");



 //============ calc. electric field dependence ========
 //*
 MAX_ITER_N = 6000000;
 thermalize_gradually(2.5, 0.02, 0.95);
 calc_rho_vs_field2(0.02, 0.01, 0.85, 0.025, "rho_vs_E.txt");
 //calc_rho_vs_field(0.02, 0.0001, 0.02, 0.002, "rho_vs_E.txt");
 //*/



 //============ a test ===========
 //calc_typical_vs_T(0.01, 2.5, 0.02, 0.95, "typical.txt");  


 //============ export flow map ===========
 /*
 alpha = 0.0001;
 double kT = 0.02;
 char fname[64];
 snprintf(fname, sizeof(fname), "map_%.4f.ppm", kT);
 thermalize_gradually(2.5, kT, 0.95);
 calc_bij();
 calc_n_iter();
 cout << "Nr. of iterations: " << iter_n << endl;
 cout << "Nr. of active nodes: " << active_n << endl;
 export_flow_map_ppm(fname, 200, 12);
 */

 return 0;
}
