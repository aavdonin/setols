import numpy as np
from numpy.linalg import norm

N = 3000 # nr. of sites in a cell (N>3!)
Na = max(2, N//20) # number of nodes in arrays "a,c", usually 5%, min. 2
Nlim = 25

#====================================================
# make conductance matrix
def mkG(r1, r2, a):
  r1 = r1[:, np.newaxis] #  temporary view
  # subtract with broadcasting
  rij = np.linalg.norm(r1-r2, axis=2) #norm along 3rd axis
  return np.exp(-rij*2/a) * (rij*2/a)**2.0
  #return np.exp(-rij*2/a) # use resisstances without prefactor

#====================================================
# limit connections
def lim_con(r1, r2, G, symmetrize):
  r1 = r1[:, np.newaxis] #  temporary view
  # subtract with broadcasting
  rij = np.linalg.norm(r1-r2, axis=2) #norm along 3rd axis
  for i in range(r2[:,0].size):
    vs = np.sort(rij[:,i])
    v = vs[Nlim+1]
    rij[:,i][rij[:,i]>v]=0
    G[rij==0]=0

  if (symmetrize):
    for i in range(r1[:,0].size):
      for j in range(i+1, r2[:,0].size):
        if (rij[i,j]*rij[j,i]==0):
          G[i,j] = G[j,i] = 0
  return G  

#====================================================
def mkMatrix(a):
  r = np.random.rand(N,3) # create a random unit-size cell
  r = r * N**(1.0/3) # scale it
  idx = np.argsort( r[:,0] ) # sort along x - axis 
  r = r[idx]
  # split sites into three sets a,b,c
  # a,c - contacts sets; b - body of the cell
  ra = r[0:Na]
  rc = r[-Na:]
  rb = r[Na:-Na]
  # prepare conductance matrices
  Ga = mkG(rb, ra, a)
  Gb = mkG(rb, rb, a)
  Gc = mkG(rb, rc, a)
  np.fill_diagonal(Gb, 0)
  
  # limit the num. of connections
  #Ga = lim_con(r[Na:-Na], r[0:Na], Ga, False)  
  #Gb = lim_con(r[Na:-Na], r[Na:-Na], Gb, False)
  #Gc = lim_con(r[Na:-Na], r[-Na:], Gc, False)
  
  Sa = np.sum(Ga, axis=1, keepdims=False)
  Sb = np.sum(Gb, axis=1, keepdims=False)
  Sc = np.sum(Gc, axis=1, keepdims=False)
  S = Sa+Sb+Sc
  return r, Ga, Gb, Gc, S, Sa, Sb, Sc

#===================================================
def calc(a):
  x = 1.0/a
  r, Ga, Gb, Gc, S, Sa, Sb, Sc = mkMatrix(a)
  M = S*np.identity(N-2*Na) - Gb
  # solve using linear algebra (LAPACK)
  f = np.linalg.solve(M, Sc) # calc potential of sites
  i1 = np.sum((f-0)*Sa) # current via contact 1
  i2 = np.sum((1-f) *Sc) # current via contact 2
  iall = (f @ Gb - f*Sb + (0-f)*Sa + (1-f)*Sc ) # curr. error
  ierrmax = np.max(np.abs(iall)) # max curr. error
  ierrsum = np.sum(np.abs(iall)) # sum of curr. error
  rhoa  = x*N**(1.0/3)/ ((i1+i2)/2) / 0.9 # rho/a; 0.9=1.0-2*0.05

  
  # the following fragment is not required!, 
  # it uses Joule heat to calculate rho, instead of Ohm's law
  # calc fij
  fm = f[:, np.newaxis] #  convert to matrix
  fij = fm-f
  Qb  = np.sum(Gb*(fij**2))/2
  fa = np.zeros(Na)
  fij = fm-fa
  Qab = np.sum(Ga*(fij**2))
  fb = np.ones(Na)
  fij = fm-fb
  Qbc = np.sum(Gc*(fij**2))
  Q = Qb+Qab+Qbc
  rhoaJ = x*N**(1.0/3)/Q / 0.9 # rho/a via Joule law
  
  # calc the contribution of the critical resistor to overall current
  # calc current in each resistor
  i_ij = fm = f[:, np.newaxis] #  convert to matrix
  fij = fm-f
  imax = np.max(np.abs(fij*Gb)) 
  #print(imax, i1, imax/i1*100)
  ok = 2*abs(i1-i2)/(i1+i2)<0.10 and 2*abs(ierrsum)/(i1+i2)<0.10  and (rhoa>0)
  
  fmtstr = "{:12.3e}{:12.3e}{:12.3e}{:12.3e}{:12.3e}{:12.3e}{:12.3e}{:>12s}"
  print(fmtstr.format(x, rhoa, rhoaJ, i1, i2, ierrmax, ierrsum, str(ok).lower()))
  
  return ok, x, rhoa, imax/i1*100

#===================================================
# output the result in form of list for easier plotting
def print_nice(d, format_str):
  print("[", end='')
  for x in d:
    print((format_str+", ").format(x), end='') 
  print("]")

# List of input localization radii (in units of d_0)
a_in = np.array([0.05, 0.051, 0.052, 0.053, 0.054, 0.055, 0.056, 0.057, 0.0574, 0.0578, 0.06, 0.062, 0.065, 0.066, 0.067, 0.07, 0.072, 0.074, 0.076, 0.078, 0.082, 0.092, 0.105, 0.115, 0.128, 0.14, 0.173, 0.19, 0.23, 0.28, 0.33, 0.39, 0.47, 0.57, 0.67, 0.77, 0.87, 0.97, 1.3, 2.1, 3.9, 7.9, 12.0, 16.0])

#
#NP = 20 # number of points
#a_in = np.array([1.0/(20.0+x/NP*5) for x in range(NP,-1,-1)])

#Np = 100 # number o fpoints
#a_in = [(1.0/8.0 +i/(Np-1)*(20.0-1.0/8.0))**(-1) for i in range(Np)]

fmtstr = "{:>12s}{:>12s}{:>12s}{:>12s}{:>12s}{:>12s}{:>12s}{:>12s}"
print(fmtstr.format("d_0/a", "rho/a", "rho/a, Jl", "i1", "i2", "i er.max", "i er.sum", "status"))

#a_in = [1.0/20.0 for x in range(50)]

x_out = []
rhoa_out = []
i_ratio = []

for ax in a_in:
  ok, x0, rhoa0, i_rat = calc(ax)
  if (ok):
    x_out.append(x0)
    rhoa_out.append(rhoa0)
    i_ratio.append(i_rat)

  
print("d0/a = ")  
print_nice(x_out, "{:.3f}")
print("rho/a = ")
print_nice(rhoa_out, "{:.2e}")
#print("Avr. contrib. of the critical current: {:.2f}%".format(sum(i_ratio)/len(i_ratio)))



