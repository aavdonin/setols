/*
    SETOLS stands for Simulation of Electron Transport Over Localized States.
    Copyright (C) 2020 Andrei Avdonin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// This module contains functions related to mean field approach

#ifndef cell_cpp
#define cell_cpp

#define ei  nodes[i].e
#define ej  nodes[j].e
#define ni  nodes[i].n
#define nj  nodes[j].n
#define rij nodes[i].r[j] 
#define rji nodes[j].r[i]
#define xi  nodes[i].p.x
#define xj  nodes[j].p.x
#define yi  nodes[i].p.y
#define yj  nodes[j].p.y

// *******************************************************************
struct tPoint{
  double x, y, z;
};

// *******************************************************************
struct tNode{
  tPoint p; // position 
  double f; // random potential due to ions (\phi)
  double n; // occupation [0...1]
  double n0; // old value of occupation
  double e; // energy of the site
  vector<double> r; // shortest distances to other nodes in CBC case
  vector<double> R; // shortes distances to traps in CBC case
  vector<double> b; // coefficients of charge balance equations
};

// *******************************************************************
// nodes - array of primary impurity sites (donors for n-type)
// traps - array of compensating impurity sites (acceptors for n-type)
int nn; // number of nodes
int nt; // number of traps
double K; // compensation coeff., 0<K<1, ( N_a/N_d for n-type)
double L; // cell size (side length of the cube)
double beta; // 1/kT in units of e^2/chi/d0
double alpha; // param proprtional to electric field: q*d0*E = alpha*e0
double aB; // localization radius (in units of d0)
double octh = 1e-10; // occupation threshold
double ocerr = 1e-10; // occupation error
int iter_n; // number of iterations in calculation of occupation
int MAX_ITER_N = 6000;
int MAX_ITER_E = 2000;
int active_n; // nr. of nodes considered for carrying current (active)  
vector<tNode>    nodes;
vector<tPoint>   traps;


// *******************************************************************
// random number between -0.5 and +0.5
double random05(){return (double) rand() / RAND_MAX - 0.5; }

// *******************************************************************
tPoint random_point(){
  tPoint p;
  p.x = L * random05();
  p.y = L * random05();
  p.z = L * random05();
  return p;
}

// *******************************************************************
double min_dist(tPoint p1, tPoint p2){
  double dx = abs(p2.x - p1.x);
  double dy = abs(p2.y - p1.y);
  double dz = abs(p2.z - p1.z);
  // this is optional if CBC are not required
  dx = min(dx, L-dx); // apply CBC, PRB 68 (2003) 184205
  dy = min(dy, L-dy);
  dz = min(dz, L-dz);
  return sqrt(dx*dx + dy*dy + dz*dz);
}

// *******************************************************************
// calculate shortest distances using Cyclic Boundary Conditions CBC
// return minimal distance between localized sites
// *******************************************************************
double calc_dist_CBC(){
  double rmin = 1e10;
  double r;

  for (int i=0; i<nn; i++){
    nodes[i].r[i] = 0; // dist to itself
    
    for (int j=i+1; j<nn; j++){ // dist to other nodes
      r = min_dist(nodes[i].p, nodes[j].p);
      rij = rji = r;
      rmin = min(r, rmin);
    }


    for (int j=0; j<nt; j++){ // dist to traps
      r = min_dist(nodes[i].p, traps[j]);
      nodes[i].R[j] = r;
      rmin = min(r, rmin); 
    }
  }
  return rmin;
}

// *******************************************************************
//  energy of interaction with all ions
// *******************************************************************
void calc_ion_energies(){
  double f;
  for (int i=0; i<nn; i++){
    f = 0; 
    for (int j=0; j<nt; j++) f += 1/nodes[i].R[j]; // potential of traps
    for (int j=0; j<nn; j++) 
      if (j != i) f -= 1/nodes[i].r[j]; // potetntial of nodes
    nodes[i].f = f;
  }
}

// *******************************************************************
// calc energies of nodes using occupation numbers
// *******************************************************************
void calc_e(){
  double e;
  for (int i=0; i<nn; i++){
    e = nodes[i].f; 
    for (int j=0; j<nn; j++) 
      if (j != i) e += ni/rij;
    ei = e;
  }
}

// *******************************************************************
// calc occupation of nodes using their energies
// *******************************************************************
void calc_n(double mu){
  for (int i=0; i<nn; i++) 
    ni = 1/(1+exp(beta*(ei-mu)));
}

// *******************************************************************
// electrical neutrality condition is satisfied when this fun. is zero
// mu - Fermi energy
// *******************************************************************
double f_el_neutr(double mu){
  double f = -nn*(double)(nn-nt)/(double)nn; //(1.0-K);
  for (int i=0; i<nn; i++){
    f += 1/(1+exp(beta*(nodes[i].e-mu)));
  }
  return f;
}

// *******************************************************************
// solve Eq f_el_neutr(x)=0 by bisection method
// *******************************************************************
double calc_Fermi_en(){
  double x1 = -10;
  double x2 = +10;
  double f1 = f_el_neutr(x1);
  double f2 = f_el_neutr(x2);
  double x, f;
  int iter = 0;
  int MAX_ITER = 100;
  double ERROR_GOAL = 1e-6;

  if (f1*f2>0){ 
    cout << "Electrical neutrality eq. has no solution!";
    return 0;
  };

  do{
    x = (x1+x2)/2;
    f = f_el_neutr(x);
    if (f*f1<=0) {x2 = x; f2 = f;};
    if (f*f2<=0) {x1 = x; f1 = f;};
    iter++;
    if (iter > MAX_ITER){
      cout << "Electr. neutrality eq.: reached iteration limit!";
      break;
    };
  } while ((x2-x1) > ERROR_GOAL);
  //cout << "Electr. neutrality eq. solved in " << iter << " iterations\n";
  return (x1+x2)/2;
}

// *******************************************************************
void print_n(){
  for (int i=0; i<nn; i++)
    cout << i << ":" << ni << "\n";
  double s=0; // sum
  for (int i=0; i<nn; i++) s += ni;
  cout << "sum n = " << s << endl; 
  cout << "----------\n";
}

// *******************************************************************
// calc approximated energy of a node using self-consistent formula
// i - index of node, mu - Fermi energy
// return difference between the new and old val. (error)
// *******************************************************************
inline double calc_e_iter(int i, double mu){
  double e0; // old value
  double e; // new value
  e0 = nodes[i].e;  
  e = nodes[i].f; 
  for (int j=0; j<nn; j++) 
    if (j != i) e += 1/nodes[i].r[j]/(1+exp(beta*(nodes[j].e-mu)));
  nodes[i].e = e;
  return abs(e - e0);
}

// *******************************************************************
// calculate energies self consistently using iterative procedure
// *******************************************************************
void calc_e_iter(){
  double maxerr; // max error 
  double mu;
  int iter = 0;
  double ERROR_GOAL = 1e-6;

  mu = calc_Fermi_en();

  do{
    maxerr = 0; 
    for (int i=0; i<nn; i++)
      maxerr = max(maxerr, calc_e_iter(i, mu));
    mu = calc_Fermi_en();
    iter++;
    if (iter > MAX_ITER_E){
      cout << "Calc. energy: reached iteration limit!\n";
      break;
    };
  } while (maxerr > ERROR_GOAL);
}

// *******************************************************************
// precalc. coefficients b_ij for all pairs of nodes
// a - localization (Bohr) radius in units of d0
// alpha - chracterizes intensity of electric field based on
// ratio of electr. field energy at dist. d0 and e0 (d0=1, e0=1)
// (i.e. ratio between external and internal field)
// q*d0*E = alpha*e0
// *******************************************************************
void calc_bij(){
  double de; // energy barrier
  double dx; // separation along field direction

  for (int i=0; i<nn; i++){
    for (int j=0; j<nn; j++){
      if (i==j){ nodes[j].b[i]=0; continue; };
      de = ej-ei; 
      dx = xj-xi;
      if ( abs(dx) > L/2.0) dx = (L-abs(dx))*copysign(1.0, -dx); // CBC
      de += alpha*dx; 

      double x = beta*abs(de);
      double y = 2.0*rij/aB;
      double t = exp(x)-1;
      if (t>0) nodes[i].b[j] = x*y*y/exp(y)/(exp(x)-1.0);
      if (t==0) nodes[i].b[j] = y*y/exp(y);
      if (de<0) nodes[i].b[j] = x*y*y/exp(y)/(1.0-1/exp(x));
      if (isnan(nodes[i].b[j])) cout << "NaN bij\n";
      if (isinf(nodes[i].b[j])) cout << "INF bij\n";
    }
  }
}

// *******************************************************************
// keep the sum of occupation numbers constant
// *******************************************************************
void rescale_n(){
    double sp=0, sm=0; 
    double dn;

    for (int i=0; i<nn; i++){
      dn = ni-nodes[i].n0; 
      if (dn>0) sp += dn; else sm -= dn;
    }

    for (int i=0; i<nn; i++){
      if (sp > sm){ // scale down positive deltas
        dn = ni-nodes[i].n0; 
        if (dn>0) ni = nodes[i].n0 + dn*sm/sp;
      }
      if (sp < sm){ // scale down amplitude of negative deltas
        dn = ni-nodes[i].n0; 
        if (dn<0) ni = nodes[i].n0 + dn*sp/sm;
      }
      // check n_i is in range [0,1]
      if (ni<0) cout << "ni<0!" <<endl;
      if (ni>1) cout << "ni>1!" <<endl;
    }
}

// *******************************************************************
inline bool fixed_occ(int i){
  return (nodes[i].n < octh) || (nodes[i].n > 1-octh);
}

// *******************************************************************
// calc approximated occupation of a node from charge conservation
// principle, iteratively
// i - index of node
// return difference between the new and old val. (error)
// *******************************************************************
inline double calc_n_iter(int i){
  double n0; // old value
  double Sa=0, Sb=0; // sums
  nodes[i].n0 = n0 = ni;
  if (fixed_occ(i)) return 0;
  for (int j=0; j<nn; j++) 
    if (j != i) {
      if (fixed_occ(j)) continue;
      Sa += nj*nodes[j].b[i];
      Sb += (1.0-nj)*nodes[i].b[j];
    }
  ni = Sa/(Sa+Sb);
  return abs(ni - n0);
}

// *******************************************************************
void count_active_nodes(){
  active_n = 0;
  for (int i=0; i<nn; i++){
    if (fixed_occ(i)) continue;
    active_n++;
  }
}

// *******************************************************************
// calculate occupation numbers using iterative procedure
// *******************************************************************
void calc_n_iter(){
  double maxerr; // max error 
  double ERROR_GOAL = ocerr;

  iter_n = 0;
  do{
    maxerr = 0; 
    for (int i=0; i<nn; i++)
      maxerr = max(maxerr, calc_n_iter(i));
    rescale_n();
    iter_n++;
    if (iter_n >= MAX_ITER_N){
      cout << "Calc. occupation: reached iteration limit!\n";
      break;
    };
  } while (maxerr > ERROR_GOAL);
  count_active_nodes();
}

// *******************************************************************
// calc electron flow between nodes i->j
// *******************************************************************
inline double fij(int i, int j){
  return nodes[i].b[j]*ni*(1-nj) - nodes[j].b[i]*nj*(1-ni);
}

// *******************************************************************
// calc electron flow through a crossection x=0
// *******************************************************************
double calc_flow(){
  double x0=0;
  double f=0; // flow
  double dx;

  for (int i=0; i<nn; i++){
    if (fixed_occ(i)) continue;
    for (int j=i+1; j<nn; j++){
      if (fixed_occ(j)) continue;
      dx = xj-xi;
      if (abs(dx)>L/2.0) continue;
      if ((xj-x0)*(xi-x0)>0) continue;
      if ((xi==x0) && (xj<=0)) continue; // (*!!) only out-going flow (1/2) if one node is at x0
      if ((xj==x0) && (xi<0)) continue; // ... otherwise the flow of this node will be counted twice
      f += fij(i,j)*dx/abs(dx);
    }
  }
  return -f; // inverted because flow of negative charge was calculated
}

// *******************************************************************
// find equilibrium occupation for given temperature (beta)
// *******************************************************************
void thermalize(){
  calc_e_iter();
  double mu = calc_Fermi_en();
  calc_n(mu);
}

// *******************************************************************
// calc resistivity
// *******************************************************************
double calc_rho(){
  thermalize();
  calc_bij();
  calc_n_iter();
  return alpha*beta*L*L/calc_flow();
}

// *******************************************************************
// calc typical values of r,e: Simle averaging test
// *******************************************************************
void calc_typical(double &r, double &e){
  double s = 0; // sum of flow
  r = 0; // typical r
  e = 0; // typical e
  for (int i=0; i<nn; i++){
    if (fixed_occ(i)) continue;
    for (int j=i+1; j<nn; j++){
      if (fixed_occ(j)) continue;
      double f = abs(fij(i,j));
      s += f;
      r += rij*f;
      e += abs(ej-ei)*f;
    }
  }
  r /= s;
  e /= s;
  
}

// *******************************************************************
// n - nr. of nodes in the cell
// k - compensation coefficient
// *******************************************************************
void create_cell(int n, double k){
  nn = n;
  nt = round(n*k); 
  K = k;
  L = pow(nn, 1.0/3.0); // cell size in units of d0

  cout << "cell size [units of d0]: " << L << "\n";
  cout << "nr. of compensating defects: " << nt << "\n"<< flush;

  nodes.resize(nn);
  traps.resize(nt);
  for (int i=0; i<nn; i++){
      nodes[i].b.resize(nn);  
      nodes[i].r.resize(nn);
      nodes[i].R.resize(nt);
  }
  // create random configurations until distances between all pairs
  // of defects are greater than 1e-4*d0
  double rmin;
  do{
    for (int i=0; i<nn; i++) nodes[i].p = random_point();
    for (int i=0; i<nn; i++) nodes[i].n = (double)(nn-nt)/(double)nn;
    for (int i=0; i<nt; i++) traps[i] = random_point();
    
    rmin = calc_dist_CBC();
  } while (rmin<1e-4);

  calc_ion_energies();
  calc_e();
}

// *******************************************************************
void save_cell(string fname){
  ofstream f;
 
  f.open(fname.c_str());
  f << "# Number of nodes \n";
  f << nn << "\n";
  f << "# Compensation coefficient \n";
  f << K << "\n";
  // save nodes
  f << "# list of nodes: index, x, y, z, fi, e, n\n";
  for (int i=0; i<nn; i++){ 
     f << setw(5) << i << " ";
     f << setw(15) << setprecision(10) << nodes[i].p.x << " ";
     f << setw(15) << setprecision(10) << nodes[i].p.y << " ";
     f << setw(15) << setprecision(10) << nodes[i].p.z << " ";
     f << setw(15) << setprecision(10) << nodes[i].f << " ";
     f << setw(15) << setprecision(10) << nodes[i].e << " ";
     f << setw(10) << setprecision(7) << nodes[i].n << "\n";
  }
  // save traps
  f << "# list of traps: index, x, y, z\n";
  for (int i=0; i<nt; i++){ 
     f << setw(5) << i << " ";
     f << setw(15) << setprecision(10) << traps[i].x << " ";
     f << setw(15) << setprecision(10) << traps[i].y << " ";
     f << setw(15) << setprecision(10) << traps[i].z << "\n";
  };
  f.close();
}

// *******************************************************************
// export DOS to file, by averaging over given nr of cells
// fname: file name
// beta_val: 1/kT 
// nr_nodes: number of nodes
// compens: coeff. of compensation
// nr_cells: number of cells for averaging of DOS
// emin, emax: interval of energies in which DOS will be calculated
// nr_intervals: nr of points in the DOS(E) curve
// *******************************************************************
void export_dos(string fname, double beta_val, 
                 int nr_nodes, double compens, int nr_cells,
                 double emin, double emax, int nr_intervals){
  beta = beta_val;
  vector<double> dos(nr_intervals, 0);
  double estep = (emax-emin)/nr_intervals;

  for (int i=0; i<nr_cells; i++){
    cout << endl << "processing cell " << i+1 << endl; 
    create_cell(nr_nodes, compens);
    thermalize();
    double mu = calc_Fermi_en();
    for (int j=0; j<nn; j++){
      int idx = trunc( (nodes[j].e-mu-emin)/estep );
      if ((idx>=0) && (idx<nr_intervals))
        dos[idx] = dos[idx] + 1;
    }
  }

  ofstream f;
  f.open(fname.c_str());
  for (int i=0; i<nr_intervals; i++){
    f << setw(15) << setprecision(10) << emin+(i+0.5)*estep << " ";
    f << setw(5) << dos[i]/nn/estep/nr_cells << endl;
  }
  f.close();

}

// *******************************************************************
bool sort_z(int i, int j){return nodes[i].p.z < nodes[j].p.z;};
// *******************************************************************
void export_pos(string fname){
  vector<double> z(nn); // vector of z-components
  vector<int> k(nn); // vector of "sorted" indexes

  for (int i=0; i<nn; i++){
    z[i] = nodes[i].p.z; // copy z-component
    k[i] = i;
  }
  sort (k.begin(), k.end(), sort_z);
  ofstream f;
  f.open(fname.c_str());
  f << "var n=[\n"; 
  for (int i=0; i<nn; i++){
     int j = k[i];
     f << "  {";
     f << "x:" << setw(15) << setprecision(10) << nodes[j].p.x*2/L << ", ";
     f << "y:" << setw(15) << setprecision(10) << nodes[j].p.y*2/L << ", ";
     f << "z:" << setw(15) << setprecision(10) << nodes[j].p.z*2/L << ", ";
     f << "n:" << setw(10) << setprecision(7) << nodes[j].n;
     f << "}"; 
     if (i< (nn-1)) f << ",";
     f << "\n";  
  }
  f << "];\n";
  f.close();
}

// *******************************************************************
double max_flow(){
  double I;
  double Imax = 0;
  for (int i=0; i<nn; i++){
    if (fixed_occ(i)) continue;
    for (int j=i+1; j<nn; j++){
      if (fixed_occ(j)) continue;
      I = abs(nodes[i].b[j]*ni*(1-nj) - nodes[j].b[i]*nj*(1-ni));
      Imax = max(I, Imax);
    }
  } 
  return Imax;
}

// *******************************************************************
double shift_cbc(double p, double p0){
  p = p-p0;
  if (p>=L/2) p = p-L;
  if (p<-L/2) p = p+L;
  return p;
}

// *******************************************************************
// find repetition of p2 (due to CBC) which is closest to p1
double closest_rep(double p1, double p2){
  double dmin = abs(p2-p1);
  double pmin = p2;
  double d = abs(p2+L-p1);
  if (d<dmin) {dmin = d; pmin = p2+L;}
  d = abs(p2-L-p1);
  if (d<dmin) pmin = p2-L;
  return pmin;
}

// *******************************************************************
double clamp(double value, double vmin, double vmax){return min(max(value, vmin), vmax);}
// *******************************************************************
double dist_to_line_seg(double x,  double y,
                        double xa, double ya,
                        double xb, double yb,
                        double w){
    double xpa = x-xa;
    double ypa = y-ya;
    double xba = xb-xa;
    double yba = yb-ya;
    double d1 = xpa*xba + ypa*yba; 
    double d2 = xba*xba + yba*yba;
    double h = clamp(d1/d2, 0.0, 1.0);
    double dx = xpa-xba*h;
    double dy = ypa-yba*h;
    return max(0.0, sqrt(dx*dx+dy*dy)-w/2.0);
}

// *******************************************************************
// average dist from pixel to line segment
// x,y: coordinates of pixel
// (xa, ya) and (xb, yb): coords of ends of the line segment
// w: width of line segment
// p: size of pixel in units of "d0"
// *******************************************************************
double aver_dist(double x, double y,
                 double xa, double ya,
                 double xb, double yb,
                 double w,  double p){
     double d =  dist_to_line_seg(x-p/2, y-p/2,xa,ya,xb,yb,w);
            d += dist_to_line_seg(x+p/2, y-p/2,xa,ya,xb,yb,w);
            d += dist_to_line_seg(x-p/2, y+p/2,xa,ya,xb,yb,w);
            d += dist_to_line_seg(x+p/2, y+p/2,xa,ya,xb,yb,w); 
     return d/4.0;
  }

// *******************************************************************
// export flow intensity of given pixel
// x0, y0: pixel coordinates
// npixels : number of pixels
// f: reference to file stream
// *******************************************************************
double flow_map_pixel(double x0, double y0, int npixels, double wmax, double Imax){
  double A=0; // intensity of pixel
  double I;
  double w; // line width
  double d; // distance from pixel to line segment
  double xa, ya, xb, yb, xc, yc; // segment: start, end, center
  double x, y; // position of the shifted pixel
  double p; // pixel size
  double e; // intensity mask
  double e2; // intensity suppression coeff. if width smaller than 1 pixel
  double wm = wmax/npixels*L; // max segment width in units of "d0"
  
  // shift cell using cbc to have new zero at x0
  for (int i=0; i<nn; i++){
    if (fixed_occ(i)) continue;
    for (int j=i+1; j<nn; j++){
      if (fixed_occ(j)) continue;
      I = nodes[i].b[j]*ni*(1-nj) - nodes[j].b[i]*nj*(1-ni);
      //if (abs(I)<Imax/100) continue;
      w = wm*abs(I)/Imax;
      xa = xi;
      ya = yi; // mk projection along z
        //ya = nodes[i].p.z; // mk projection along y
      xb = closest_rep(xa, xj);
      yb = closest_rep(ya, yj); // mk projection along z
        //yb = closest_rep(ya, nodes[j].p.z); // mk projection along y
      // shift cell using cbc to have the center of the segment 
      // in the center of the cell
      xc = (xa+xb)/2;
      yc = (ya+yb)/2;
      x  = shift_cbc(x0, xc); 
      xa = shift_cbc(xa, xc);
      xb = shift_cbc(xb, xc);  
      y  = shift_cbc(y0, yc); 
      ya = shift_cbc(ya, yc);
      yb = shift_cbc(yb, yc);
      p = L/npixels;
      e2 = 1.0;
      if (w/p < 1) {e2 = w/p; w = p;} 
      d = aver_dist(x, y, xa, ya, xb, yb, w, p);
      e = d/(p/2.0);
      e = min(1.0, e);
      e = 1-e;
      if (e*e2 >= abs(A))  A = e*e2 * copysign(1.0, -I*(xb-xa));
    }
  } 
  return A;
}

// *******************************************************************
// export data to js file
// *******************************************************************
void export_flow_map(string fname, int npixels, double wmax){
  double x0, y0;
  double A;
  double Imax = max_flow();

  cout << "Max. flow: " << Imax << endl;

  ofstream f;
  f.open(fname.c_str());

  f << "var flowMap = [\n";
  for (int i=0; i<npixels; i++){
    cout << "Exporting column " << setw(5) << i+1 << " of " << npixels << endl;
    f << "[\n";
    x0 = L*((i+0.5)/ npixels - 0.5);
    for (int j=0; j<npixels; j++){
      y0 = L*((j+0.5)/ npixels - 0.5);
      A = flow_map_pixel(x0, y0, npixels, wmax, Imax);
      f << setw(15) << setprecision(7) << A;
      if (j< (npixels-1)) f << ", ";
    }
    f << "]";
    if (i< (npixels-1)) f << ",\n";
  }
  f << "];";
  f.close();
}

// *******************************************************************
// export image to ppm file
// *******************************************************************
void export_flow_map_ppm(char* fname, int npixels, double wmax){
  double x0, y0;
  double A;
  double Imax = max_flow();

  cout << "Max. flow: " << Imax << endl;

  ofstream f;
  f.open(fname, std::ios::out | std::ios::binary);
  char header[64];
  snprintf(header, sizeof(header), "P6\n%d %d\n255\n", npixels, npixels);
  f <<  header;

  for (int i=0; i<npixels; i++){
    cout << "Exporting column " << setw(5) << i+1 << " of " << npixels << endl;

    y0 = L*((i+0.5)/ npixels - 0.5);
    for (int j=0; j<npixels; j++){
      x0 = L*((j+0.5)/ npixels - 0.5);
      A = flow_map_pixel(x0, y0, npixels, wmax, Imax);
      static unsigned char color[3];
      if (A>=0){
        color[0] = 255;
        color[1] = round((1-A) * 255);
        color[2] = round((1-A) * 255);
      } else { 
        color[0] = round((1-abs(A)) * 255);
        color[1] = round((1-abs(A)) * 255);
        color[2] = 255;
      }
      f << color[0] << color[1] << color[2];
    }
  }
  f.close();
}

#endif 

