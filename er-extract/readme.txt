 This program "er-extract" is part of SETOLS package
 It extracts the effective hopping energy and distance
 from a temperature dependence of resistivity in hopping regime.
 
 It requires one input file and generates one output file.
 Three parameters should be set before running the program.
 	1. input file name
 	2. output file name 	
 	3. mode: "EXACT" or "APPROX"
 	
 "EXACT" mode is sutable for data obtained using computer simulations
 or theoretically.
 "APPROX" mode can be used with both the simulated data and data
 obtaind experimentally
 
 ====================
 Output file format:
 ====================
 first line: value of localization length "a" in units of d0 
 (in APPROX mode "a" can be set to 1.0)
 other lines should contain rows of a table with 3 columns:
 	1. temperature "T"
 	2. column "t" (in APPROX mode can be set to 1.0 or resistivity)
 	3. column "g" g=(ln \rho)'
! The rows must be sorted from high tempearture (top line) to low
temperature (bottom line) in the EXACT mode.

 ====================
 Output file format:
 ====================
 output has 3 columns
 	1. temperature "T"
 	2. effective hopping energy "e" [in units of temperature]
 	3. effective hopping distance "r" [in units of d0] 
 	(in APPROX mode this column can be disregarded)
 	
Two examples of input and output files are provided with the source code.
One was generated using simulations made using SETOLS mean field approach
and the other was produced from experimental data for ZnO.

More detailed information can be provided if there will be any demand.
avdonin@ifpan.edu.pl
a.avdonin.76@gmail.com
