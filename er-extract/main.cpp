/*
    SETOLS stands for Simulation of Electron Transport Over Localized States.
    Copyright (C) 2020 Andrei Avdonin
    avdonin@ifpan.edu.pl
    a.avdonin.76@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
 This program "er-extract" is part of SETOLS package
 It extracts effective hopping energy and distance
 from a temperature dependence of resistivity in hopping regime. 
 Usage is described in the attached readme file.
*/ 

#include <iostream>  // cout
#include <string>
#include <vector>
#include <cstdlib>   // exit, rand, srand, RAND_MAX
#include <sstream>   // ostringstream, istringstream
#include <fstream>   // ofstream, ifstream
#include <iomanip>   // setprecision, setfill, setw
#include <cmath>     // exp, pow, sqrt

typedef unsigned uint;
using namespace std;

double a=0; // localization radius (provided from input file)
vector<double> T, t, g, vx, vy;
ofstream f2;

/**********************************************************/
uint linecount(string fname){
  ifstream f; 
  double T, R, Rp;
  uint c=0;
  f.open(fname.c_str());
  while (f >> T >> R >> Rp) { c++;}
  f.close(); 
  return c;
}

/**********************************************************/
void loadfile(string fname){
  ifstream f; 
  f.open(fname.c_str());
  uint c = linecount(fname);
  T.resize(c);
  t.resize(c);  
  g.resize(c);
  vx.resize(c);    
  vy.resize(c);    
  
  f >> a; // load localization radius
  for (uint i=0; i<c; i++) {
    f >> T[i] >> t[i] >> g[i];
  }
  f.close(); 
}

/**********************************************************/
double yfun(double x, vector<double> p){
  double  gdT = p[1];
  double  x0 = p[2];
  double  y0 = p[3];
  double  T0 = p[4];
  double  T  = p[5];
  double  alpha = exp(x)/(exp(x)-1);
  double  f2 = T0/T*(x-x0)*(alpha-1/x);
  double  f = f2-gdT-y0-1;
  double  sqrtD = sqrt(f*f-4*y0);
  double  y1 = (-f+sqrtD)/2;
  double  y2 = (-f-sqrtD)/2;
  double  y;
  if (abs(y1-y0)<abs(y2-y0)) y = y1; else y = y2;
  return y;
}

/**********************************************************/
double err1(double x,  vector<double> p){
   double gT = p[0];
   double ex = exp(x);
   double alpha = ex/(ex-1.0);
   return pow(x*alpha+gT-1.0, 2.0);
}

/**********************************************************/
double err2(double y,  vector<double> p){
  double t = p[0];
  double x = p[1];
  return pow(exp(y)*(exp(x)-1)-t*x*y, 2.0);
}

/**********************************************************/
double err3(double x,  vector<double> p){
  double t = p[0];
  double y = yfun(x, p);
  return pow(t*x*y-exp(y)*(exp(x)-1), 2.0);
}

/**********************************************************/
void findmin1D(double &x, vector<double> p,  
               double (*f)(double, vector<double>)){
  double sgn; // sign
  double dx = 1e-8; // step for gradient calculation
  double step = x/20; // descent step
  while (true) {
       double f1 = f(x, p);
       double f2 = f(x+dx, p);
       if (f2 < f1) sgn = 1; else sgn = -1;
       // make a step in the gradient direction
       f2 = f(x+sgn*step, p);
       if (f2 < f1) x = x+sgn*step;
       else step = step/2.0;
       if (step < dx*10) break;
  }
}

/**********************************************************/
void output(double T, double e, double r){
  cout << setw(12) << setprecision(7) << T;  
  cout << setw(20) << setprecision(12) << e;
  cout << setw(20) << setprecision(12) << r << endl;
  
  f2 << setw(12) << setprecision(7) << T;  
  f2 << setw(20) << setprecision(12) << e;
  f2 << setw(20) << setprecision(12) << r << endl;      
}

/**********************************************************/
// first method, disregarding derivatives
// i: index of the data point
/**********************************************************/
void calcxy1(double &x, double &y, uint i){
  vector<double> p;
  p.resize(2);
  p[0] = g[i]*T[i];
  findmin1D(x, p, &err1);  
  p[0] = t[i];
  p[1] = x;
  findmin1D(y, p, &err2);   
  output(T[i], x*T[i], y*a/2.0);
  vx[i] = x;
  vy[i] = y;
}

/**********************************************************/
// second method, accounting for derivatives
// i: index of the new data point
// j: index of the old data point
/**********************************************************/
void calcxy2(double &x, double &y, uint i, uint j){
  vector<double> p;
  p.resize(6);
  p[0] = t[i];
  p[1] = g[i]*(T[i]-T[j]);
  p[2] = x; // old value, before calculation
  p[3] = y; // old value
  p[4] = T[j];
  p[5] = T[i];
  findmin1D(x, p, &err3);
  y = yfun(x,p);   
  output(T[i], x*T[i], y*a/2.0);  
  vx[i] = x;
  vy[i] = y;  
}

/**********************************************************/
/*                        MAIN                            */
/**********************************************************/
int main( int argc, char** argv)
{
  string in_file_name  = "002_tg_vs_T_MF_500at_sym.txt";
  string out_file_name = "002_er_vs_T_MF_500at_sym.txt";  
  string mode = "EXACT"; // "EXACT" or "APPROX"
  
  loadfile(in_file_name); // input file
  f2.open(out_file_name); // output file

  double x = 0.5;
  double y = 2/a;

  uint j=0;
  calcxy1(x, y, j);
  for (uint i=1; i<T.size(); i++) {
    if (mode=="APPROX") calcxy1(x, y, i);
    if (mode=="EXACT") {
      calcxy2(x, y, i, j);
      j = i;
    }
  }    
  f2.close();

/*  
  // output for testing. ignore...
  for (uint i=1; i<T.size(); i++) {
    double xp = (vx[i]-vx[i-1])/(T[i]-T[i-1])*T[i-1]/T[i];
    double yp = (vy[i]-vy[i-1])/(T[i]-T[i-1]);    
    double alpha = exp(vx[i])/(exp(vx[i])-1);
    double err = 100*(g[i]-yp*(1-1/vy[i])-xp*(alpha-1/vx[i]))/g[i];
    cout << T[i] << "  " << err << endl;
  }
*/
  
  
  cout << "DONE!" << endl;
  return 0;
}
